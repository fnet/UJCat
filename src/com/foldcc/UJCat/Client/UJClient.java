package com.foldcc.UJCat.Client;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

public class UJClient {
    private String host = "127.0.0.1";
    private int port = 8099;
    private Class<? extends ClientService> cs = ClientService.class;
    private EventLoopGroup group = new NioEventLoopGroup();


    public void ClientSeting(Class<? extends ClientService> c, int p, String ip) {
        this.host = ip;
        this.port = p;
        this.cs = c;
        System.out.println("客户端初始化...");
    	System.out.println("端口: "+ p);
    	System.err.println("指向IP: " + ip);
    	System.out.println("业务模板 : " + c.getName());
    }

    public void ClientStart() {
        try {
            Bootstrap b = new Bootstrap();
            ((Bootstrap)((Bootstrap)b.group(this.group)).channel(NioSocketChannel.class)).handler(new ClientInitializer(this.cs));
            b.connect(this.host, this.port).sync();
        } catch (InterruptedException var2) {
            var2.printStackTrace();
        }

    }

    public void closeSocket() {
        this.group.shutdownGracefully();
    }
}
