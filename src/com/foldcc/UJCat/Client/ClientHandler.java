package com.foldcc.UJCat.Client;


import com.foldcc.UJCat.Server.EncodeAndDecode;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.SocketChannel;

public class ClientHandler extends SimpleChannelInboundHandler<String> {
    final String headString = "&%&";
    private boolean isVerify = false;
    private Channel channel = null;
    private SocketChannel socketChannel;
    private ClientService clientService = null;
    private Class<? extends ClientService> clientServiceClass = ClientService.class;
    public String clientKey = null;

    public ClientHandler(Class<? extends ClientService> C, SocketChannel socketChannel) {
        this.socketChannel = socketChannel;
        this.channel = socketChannel.pipeline().channel();
        this.clientServiceClass = C;
        this.instanceClass();
    }

    public synchronized void sendMsg(int num, String body) {
        if (this.channel == null) {
            this.channel = this.socketChannel.pipeline().channel();
//            System.out.println("get Handler");
        } else if (this.socketChannel.isActive()) {
            this.channel.writeAndFlush(num + "&%&" + body + "\r\n");
        }

    }

    public synchronized void sendMsg(String num, String body) {
        if (this.channel == null) {
            this.channel = this.socketChannel.flush();
            System.out.println("get Handler");
        }

        if (this.socketChannel.isActive()) {
            this.channel.writeAndFlush(num + "&%&" + body + "\r\n");
        }

    }

    private void instanceClass() {
        try {
            if (this.clientServiceClass != null && this.clientServiceClass != ClientService.class) {
                this.clientService = (ClientService)this.clientServiceClass.newInstance();
            } else {
                this.clientService = new ClientService() {
                    public void msgService(String type, String body) {
                        System.out.println("Client: " + body);
                    }

                    public void exception(int type) {
                        System.out.println("Client exception or close");
                    }

                    public void active() {
                        System.out.println("Client isActive");
                    }
                };
            }
        } catch (Exception var2) {
            var2.printStackTrace();
        }

    }

    protected void channelRead0(ChannelHandlerContext ctx, String msg) throws Exception {
//        System.out.println(msg);
        if (this.isVerify) {
            this.clientService.msgHandler(msg);
        } else if ((new EncodeAndDecode()).decode(msg) != null) {
            this.isVerify = true;
            this.clientKey = (new EncodeAndDecode()).decode(msg);
            ctx.writeAndFlush(this.clientKey + "\r\n");
            System.out.println("SSR验证...");
            this.clientService.setServerHandler(this , clientKey);
            this.clientService.active();
        }

    }

    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("连接成功!");
        super.channelActive(ctx);
    }

    public void channelInactive(ChannelHandlerContext ctx) {
        this.clientService.exception(0);

        try {
            super.channelInactive(ctx);
        } catch (Exception var3) {
            var3.printStackTrace();
        }

    }
}
