package com.foldcc.UJCat.Client;

public class ClientServiceTest extends  ClientService {

    @Override
    public void msgService(String var1, String var2) {
        System.out.println("收到消息！ " + var2);
    }

    @Override
    public void exception(int var1) {

    }

    @Override
    public void active() {
        System.out.println("链接成功! ");
    }
    
    @Override
    protected void finalize() throws Throwable {
    	System.out.println("用户被销毁!");
    	super.finalize();
    }
}
