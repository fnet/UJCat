package com.foldcc.UJCat.Client;


public abstract class ClientService {
    public ClientHandler clientHandler = null;
    public String userKey = null;

    void setServerHandler(ClientHandler clientHandler , String Key) {
        this.clientHandler = clientHandler;
    }

    protected void msgHandler(String msg) {
        int b = msg.indexOf("&%&");
        this.msgService(msg.substring(0, b), msg.substring(b + 3, msg.length()));
    }

    public abstract void msgService(String var1, String var2);

    public abstract void exception(int var1);

    public abstract void active();
}
