package com.foldcc.UJCat.Server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class UJServer {
    private static int portNumber = 8099;

    public void setServer(int port , Class<? extends MessageSelect> select) {
        portNumber = port;
        HandlerManager.getHandlerManager().setMsgSelectClass(select);
    }

    public void ServiceStart() throws InterruptedException {
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        NioEventLoopGroup workerGroup = new NioEventLoopGroup();

        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup);
            b.channel(NioServerSocketChannel.class);
            b.childHandler(new ServerInitializer());
            ChannelFuture f = b.bind(portNumber).sync();
            System.out.println("服务器启动...");
            System.out.println("监听端口: " + portNumber);
            f.channel().closeFuture().sync();
            System.out.println("关闭端口监听");
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
            System.out.println("关闭服务器");
        }
    }
}
