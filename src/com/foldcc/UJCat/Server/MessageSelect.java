package com.foldcc.UJCat.Server;

public abstract class MessageSelect {
	
    private ServerHandler serverHandler = null;

    public String userKey = null;
    
    private Object o = new Object();
    
    
    public void sendMsg(int num, String body) {
    	synchronized (o) {
    		if(serverHandler != null) {
    			serverHandler.sendMsg(num, body);
    		}
		}
    }
    
    public void sendMsg(String num, String body) {
    	synchronized (o) {
    		if(serverHandler != null) {
    			serverHandler.sendMsg(num, body);
    		}
		}
    }
    
    public synchronized void close() {
    	if(serverHandler != null) {
    		serverHandler.socketChannel.close();
    	}
    }

    void setServerHandler(ServerHandler serverHandler1) {
        this.serverHandler = serverHandler1;
        userKey = serverHandler1.userServiceID;
    }

    protected void msgHandler(String msg) {
        int b = msg.indexOf("&%&");
        this.msgService(msg.substring(0, b), msg.substring(b + 3, msg.length()));
    }

    public abstract void msgService(String var1, String var2);

    public abstract void exception(int var1);

    public abstract void active();
}
