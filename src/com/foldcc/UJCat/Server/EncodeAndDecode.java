package com.foldcc.UJCat.Server;

import java.io.UnsupportedEncodingException;

public class EncodeAndDecode {
    private String[] encodeByte = new String[]{"@", "#", "*", "$", "+", "~", "&", "%", "(", ")", "-"};

    public EncodeAndDecode() {
    }

    public String encode(String str) {
        if (str == null) {
            return null;
        } else {
            StringBuilder sb = new StringBuilder();

            try {
                byte[] sby = str.getBytes("UTF-8");
                int n = sby.length;

                for(int i = 0; i < n; ++i) {
                    int a = sby[i];
                    char[] s = String.valueOf(a).toCharArray();
                    char[] var11 = s;
                    int var10 = s.length;

                    for(int var9 = 0; var9 < var10; ++var9) {
                        char b = var11[var9];
                        if (b != '-') {
                            sb.append(this.encodeByte[b - 48]);
                        } else {
                            sb.append(this.encodeByte[10]);
                        }
                    }

                    sb.append("_");
                }

                return sb.deleteCharAt(sb.length() - 1).toString();
            } catch (UnsupportedEncodingException var12) {
                var12.printStackTrace();
                return null;
            }
        }
    }

    public String decode(String str) {
        if (str == null) {
            return null;
        } else {
            String[] strs = str.split("_");
            int n = strs.length;
            byte[] sby = new byte[n];

            for(int i = 0; i < n; ++i) {
                char[] s = strs[i].toCharArray();
                String num = "";
                char[] var11 = s;
                int var10 = s.length;

                for(int var9 = 0; var9 < var10; ++var9) {
                    char ss = var11[var9];
                    if (ss == '-') {
                        num = num + "-";
                    } else {
                        for(int j = 0; j < this.encodeByte.length; ++j) {
                            if (this.encodeByte[j].toCharArray()[0] == ss) {
                                num = num + String.valueOf(j);
                                break;
                            }
                        }
                    }
                }

                try {
                    sby[i] = Integer.valueOf(num).byteValue();
                } catch (Exception var14) {
                    ;
                }
            }

            try {
                return new String(sby, "UTF-8");
            } catch (UnsupportedEncodingException var13) {
                var13.printStackTrace();
                return null;
            }
        }
    }
}
