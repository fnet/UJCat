package com.foldcc.UJCat.Server;


import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.SocketChannel;
import java.util.UUID;

public class ServerHandler extends SimpleChannelInboundHandler<String> {
    final String headString = "&%&";
    public SocketChannel socketChannel = null;
    public ChannelHandlerContext channelHandlerContext = null;
    public String userServiceSSR = null;
    public String userServiceID = null;
    private boolean isVerify = false;
    private MessageSelect messageSelect = null;

    public ServerHandler(SocketChannel ch) {
        this.socketChannel = ch;
        this.channelHandlerContext = this.socketChannel.pipeline().context("handler");
        this.userServiceID = UUID.randomUUID().toString().replaceAll("-", "");
        this.userServiceSSR = (new EncodeAndDecode()).encode(this.userServiceID);
        this.instanceMessageSelect();
    }

    private void instanceMessageSelect() {
        try {
            if (!HandlerManager.getHandlerManager().getMsgSelectClassName().getName().equals(MessageSelect.class.getName())) {
                Class<? extends MessageSelect> c0 = HandlerManager.getHandlerManager().getMsgSelectClassName();
                this.messageSelect = (MessageSelect)c0.newInstance();
            } else {
                this.messageSelect = new MessageSelect() {
                    public void msgService(String type, String body) {
                        System.out.println("数据ID：" + type + "数据内容: " + body);
                    }

                    public void exception(int type) {
                        System.out.println("用户异常断开 ！");
                    }

                    public void active() {
                        System.out.println("用户访问 !");
                    }
                };
            }

            this.messageSelect.setServerHandler(this);
        } catch (Exception var2) {
            var2.printStackTrace();
            this.messageSelect = new MessageSelect() {
                public void msgService(String type, String body) {
                    System.out.println(body);
                }

                public void exception(int type) {
                    System.out.println("用户异常断开 ！");
                }

                public void active() {
                    System.out.println("用户访问 !");
                }
            };
            this.messageSelect.setServerHandler(this);
        }

    }

    public void sendMsg(int num, String body) {
        if (this.channelHandlerContext == null) {
            this.channelHandlerContext = this.socketChannel.pipeline().context("handler");
        }

        if (this.socketChannel.isActive()) {
            this.channelHandlerContext.writeAndFlush(num + "&%&" + body + "\r\n");
        }

    }

    public void sendMsg(String num, String body) {
        if (this.channelHandlerContext == null) {
            this.channelHandlerContext = this.socketChannel.pipeline().context("handler");
        }
        if (this.socketChannel.isActive()) {
            this.channelHandlerContext.writeAndFlush(num + "&%&" + body + "\r\n");
        }

    }

    public synchronized boolean isActive() {
        return this.socketChannel.isActive();
    }

    protected void channelRead0(ChannelHandlerContext ctx, String msg) throws Exception {
        if (this.isVerify) {
            this.messageSelect.msgHandler(msg);
        } else if (msg.equals(this.userServiceID)) {
            System.out.println("SSR验证成功");
            this.isVerify = true;
            this.messageSelect.active();
        }

    }

    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        ctx.writeAndFlush(this.userServiceSSR + "\r\n");
        super.channelActive(ctx);
    }

    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        HandlerManager.getHandlerManager().addHandlerList(this);
        super.handlerAdded(ctx);
    }

    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        HandlerManager.getHandlerManager().removeHandlerWithServiceID(this.userServiceID);
        this.messageSelect.exception(0);
        socketChannel = null;
    	channelHandlerContext = null;
    	messageSelect = null;
        super.handlerRemoved(ctx);
    }

    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        System.out.println("ChatClient: " + this.userServiceID + " 异常");
        this.messageSelect.exception(1);
        cause.printStackTrace();
        ctx.close();
    }
}
