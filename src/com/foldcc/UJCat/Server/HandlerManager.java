package com.foldcc.UJCat.Server;



import java.util.ArrayList;
import java.util.Iterator;

public class HandlerManager {
    private ArrayList<ServerHandler> handlers = null;
    private static HandlerManager socketUserManager = null;
    private Object lockIOuser = null;
    private Class<? extends MessageSelect> msgSelectClass = MessageSelect.class;

    public Class<? extends MessageSelect> getMsgSelectClassName() {
        return this.msgSelectClass;
    }

    public void setMsgSelectClass(Class<? extends MessageSelect> msgSelectClassName) {
    	System.out.println("业务模块: " + msgSelectClassName.getName());
        this.msgSelectClass = msgSelectClassName;
    }

    public static synchronized HandlerManager getHandlerManager() {
        if (socketUserManager == null) {
            socketUserManager = new HandlerManager();
        }

        return socketUserManager;
    }

    public HandlerManager() {
        this.lockIOuser = new Object();
        this.handlers = new ArrayList<ServerHandler>();
    }

    void addHandlerList(ServerHandler serverHandler) {
        synchronized(this.lockIOuser) {
            if (!this.handlers.contains(serverHandler)) {
                this.handlers.add(serverHandler);
                System.out.println("添加用户");
            }

        }
    }

    public boolean removeHandler(ServerHandler serverHandler) {
        synchronized(this.lockIOuser) {
            if (this.handlers.contains(serverHandler)) {
                System.out.println("踢出用户 " + serverHandler.userServiceID);
                return this.handlers.remove(serverHandler);
            } else {
                return false;
            }
        }
    }

    public boolean removeHandlerWithServiceID(String userServiceID) {
        synchronized(this.lockIOuser) {
            Iterator<ServerHandler> var3 = this.handlers.iterator();
            ServerHandler su;
            do {
                if (!var3.hasNext()) {
                    return false;
                }

                su = (ServerHandler)var3.next();
            } while(!su.userServiceID.equals(userServiceID));
            System.out.println("[HandlerManager] 踢出用户 : " + su.userServiceID);
            return this.handlers.remove(su);
        }
    }

    public ServerHandler getHandlerWitchServiceID(String userServiceID) {
        synchronized(this.lockIOuser) {
            Iterator<ServerHandler> var3 = this.handlers.iterator();

            ServerHandler su;
            do {
                if (!var3.hasNext()) {
                    return null;
                }

                su = (ServerHandler)var3.next();
            } while(!su.userServiceID.equals(userServiceID));

            return su;
        }
    }
}